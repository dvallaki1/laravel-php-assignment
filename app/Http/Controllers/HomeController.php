<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function saveProfile(Request $request)
    {
        if ($request->hasFile('profile_pic')) {
            $file = $request->file('profile_pic');

            $extension = $file->getClientOriginalExtension();

            if(in_array($extension, ['jpg','jpeg','png']))
            {
                $path = public_path('storage/app/profile_pics');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $name = uniqid() . '_' . trim($file->getClientOriginalName());
        
                $file->move($path, $name);
                $profile_pic = 'profile_pics/' . $name;
            }
        } else {
            $profile_pic = User::find(Auth::id())->profile_pic;
        }

        

        User::find(Auth::id())->update(['profile_pic'=>$profile_pic]);

        return back()->with('success','Profile pic updated successfully.');
    }
}
