@if ($message = Session::get('success'))
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="alert alert-success custom-alert-success   alert-block text-center mb-0">

                    <p class="mb-0">{!! $message !!}
                        
                    </p>

                </div>
            </div>
        </div>
    </div>
@endif

@if ($message = Session::get('status'))
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="alert alert-success custom-alert-success   alert-block text-center mb-0">

                    <p class="mb-0">{!! $message !!}
                        
                    </p>

                </div>
            </div>
        </div>
    </div>
@endif

@if ($message = Session::get('error'))

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="alert alert-danger  custom-alert-danger   alert-block  text-center">

                    <p class="mb-0">{!! $message !!}
                        

                    </p>
                </div>
            </div>
        </div>
    </div>
@endif

@if (isset($errors) && $errors->any())

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="alert alert-danger  custom-alert-danger   alert-block  text-center">

                    <p class="mb-0">{!! $errors->first() !!}
                        
                    </p>
                </div>
            </div>
        </div>
    </div>

@endif

<script>
    $(".alert").each(function () {
        $(".alert").not( ".custom-alert-info").delay(6000).fadeOut(3000);
    });
</script>